#/bin/sh

git add .
git commit -m "Atualização de conteúdo - $(date +%d/%m/%Y" - "%H:%M:%S)"

git push origin data

git checkout working
git merge data

jekyll build 

scp -r _site/* actmail@plespiao.org.br:~/Plespiao.org.br/beta